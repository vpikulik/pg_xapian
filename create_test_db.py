#!/usr/bin/env python

import xapian

DB_NAME = 'test_db'

DATA = (
    {
        'id': 10,
        'data': 'word1 word2',
    },
    {
        'id': 20,
        'data': 'word1 word3',
    }
)

db = xapian.WritableDatabase(DB_NAME, xapian.DB_CREATE_OR_OVERWRITE)


for data_obj in DATA:

    indexer = xapian.TermGenerator()
    stem = xapian.Stem('english')
    indexer.set_stemmer(stem)

    doc = xapian.Document()
    indexer.set_document(doc)
    indexer.index_text(data_obj['data'])
    doc.add_value(1, str(data_obj['id']))
    db.add_document(doc)

db.close()
