
#include <iostream>
#include <stdlib.h>
#include <xapian.h>

#include "xapian_searcher.h"

using namespace std;

#define ID_VALUE_NUM 1


Xapian::MSet _get_mset(
    char* raw_query,
    char* db_path,
    char* lang,
    int limit,
    int offset
) {
    // load database
    Xapian::Database db(db_path);

    // init stemmer
    Xapian::Stem stemmer(lang);

    // init query parser
    Xapian::QueryParser query_parser;
    query_parser.set_stemmer(stemmer);
    query_parser.set_database(db);
    query_parser.set_stemming_strategy(Xapian::QueryParser::STEM_SOME);

    // get query
    Xapian::Query query = query_parser.parse_query(
        raw_query,
        Xapian::QueryParser::FLAG_PARTIAL|Xapian::QueryParser::FLAG_WILDCARD
    );

    // prepare mset
    Xapian::Enquire enquire(db);
    enquire.set_query(query);
    return enquire.get_mset(offset, limit);
}

int* _search_and_get_ids(
    char* raw_query,
    char* db_path,
    char* lang,
    int limit,
    int offset
) {
    Xapian::MSet mset = _get_mset(
        raw_query,
        db_path,
        lang,
        limit,
        offset
    );

    // prepare result
    int* results = new int[limit];

    // fill results with zeros
    for (int i=0; i<limit; i++) {
        *(results + i) = 0;
    }

    // iterate results and get ids
    int i = 0;
    for (
        Xapian::MSetIterator item=mset.begin();
        item!=mset.end();
        item++
    ) {
        Xapian::Document document = item.get_document();
        string id_string = document.get_value(ID_VALUE_NUM);
        *(results + i) = atoi(id_string.c_str());
        i++;
    }

    return results;
}

int _search_and_get_count(
    char* raw_query,
    char* db_path,
    char* lang,
    int limit,
    int offset
) {
    Xapian::MSet mset = _get_mset(
        raw_query,
        db_path,
        lang,
        limit,
        offset
    );

    return mset.get_matches_estimated();
}
